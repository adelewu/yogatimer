package com.sport.yoga;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: Hank_Chiu@htc.com
 * Date: 2013/12/14
 */
public class AboutDialog {
    private static final Object mLock = new Object();
    private static Dialog mDialog;

    private AboutDialog() {
        // Constructor.
    }

    public static void show(Context context) {
        synchronized (mLock) {
            if (mDialog == null) {
                mDialog = new Dialog(context);
            }

            if (mDialog.isShowing()) {
                return;
            }

            mDialog.setTitle(R.string.app_name);
            mDialog.setContentView(R.layout.about_dialog);

            final Button close = (Button) mDialog.findViewById(R.id.buttonClose);
            close.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                }

            });

            final TextView txtVersion = (TextView) mDialog.findViewById(R.id.txtVersion);
            try {
                final String version = txtVersion.getText() + " " +
                        context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                txtVersion.setText(version);
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(Constants.TAG, "Cannot find version info! " + e.getMessage());
            }

            final TextView rateapp = (TextView) mDialog.findViewById(R.id.txtRateapp);
            rateapp.setText(Html.fromHtml(context.getString(R.string.about_rateapp)));
            Linkify.addLinks(rateapp, Linkify.ALL);
            rateapp.setMovementMethod(LinkMovementMethod.getInstance());

            try {
                mDialog.show();
            } catch (Exception e) {
                // Fix crash issue when click "About" while activity is starting.
                Log.e(Constants.TAG, e.getMessage());
                mDialog.dismiss();
                mDialog = null;
            }
        }
    }
}
