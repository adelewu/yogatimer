package com.sport.yoga;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class MyActivity extends Activity {

    public static final String PREFS_NAME = "EasyIntervalTimerPrefs";
    public static final String PREFS_WORKOUT = "WORKOUT";
    public static final String PREFS_REST = "REST";
    public static final String PREFS_ROUNDS = "ROUNDS";
    private static final int TEXT_PADDING = 12;
    private Button mBtnWorkOut;
    private Button mBtnRest;
    private SeekBar mSeekBarRounds;
    private TextView mtvSeekBar;
    private Button mBtnStart;
    private Button mBtnReset;
    private ProgressBar mPbWorkout;
    private ProgressBar mPbRest;
    private YogaService mYogaService;
    private Status mStatus;
    private long mWorkoutTime;
    private long mRestTime;
    private long mRounds;
    private PowerManager.WakeLock mWakeLock;
    private Toast mToast;
    private long mLastBackPressTime = 0;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            mYogaService = ((YogaService.YogaBinder) service).getService();
            mYogaService.addListener(mListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mYogaService.removeListener(mListener);
            mYogaService = null;
        }
    };
    private TimerListener mListener = new TimerListener() {
        public void onChange(YogaService.EventType type, TimerData timerData) {
            if (type == YogaService.EventType.Start) {
                mStatus = Status.start;
                mBtnStart.setText(R.string.activity_button_pause);
                mSeekBarRounds.setEnabled(false);
                mWakeLock.acquire((timerData.workoutTime + timerData.restTime) * (timerData.roundCount + 1) + 20000);    //extend 20 seconds to let user can dismiss dialog
            } else if (type == YogaService.EventType.Pause) {
                mStatus = Status.pause;
                mBtnStart.setText(R.string.activity_button_start);
            } else if (type == YogaService.EventType.Tick || type == YogaService.EventType.Finish) {
                //update YogaTime
                String strWork = timeToString(timerData.workoutTime);
                mBtnWorkOut.setText(strWork);

                String strRest = timeToString(timerData.restTime);
                mBtnRest.setText(strRest);

                mSeekBarRounds.setProgress((int) timerData.roundCount);

                mPbWorkout.setProgress((int) (mWorkoutTime - timerData.workoutTime));
                mPbRest.setProgress((int) (mRestTime - timerData.restTime));

            } else if (type == YogaService.EventType.Complete) {
                reset();
            }

        }
    };

    @Override
    public void onBackPressed() {
        if (this.mLastBackPressTime < (SystemClock.elapsedRealtime() - Constants.CLOSE_APP_TIMEOUT)) {
            mToast = Toast.makeText(this,
                    String.format(getString(R.string.toast_close_app_msg), getString(R.string.app_name)),
                    Toast.LENGTH_LONG);
            mToast.show();
            this.mLastBackPressTime = SystemClock.elapsedRealtime();
        } else {
            if (mToast != null) {
                mToast.cancel();
            }
            super.onBackPressed();
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //get preference data
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        mWorkoutTime = prefs.getInt(PREFS_WORKOUT, 15000);
        mRestTime = prefs.getInt(PREFS_REST, 5000);
        mRounds = prefs.getInt(PREFS_ROUNDS, 3);

        //init status
        mStatus = Status.stop;

        //Initial workout button
        mBtnWorkOut = (Button) findViewById(R.id.btnWorkoutTime);
        mBtnWorkOut.setText(timeToString(mWorkoutTime));

        // Set ic_pencil icon to the right of time.
        final Drawable pencil = getResources().getDrawable(R.drawable.pencil);
        mBtnWorkOut.setCompoundDrawablesWithIntrinsicBounds(null, null, pencil, null);
        mBtnWorkOut.setCompoundDrawablePadding(TEXT_PADDING);

        mBtnWorkOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //launch TimerPicker
                TimePickerDialog dialog = new TimePickerDialog(MyActivity.this);
                dialog.setTimeChangeListener(new TimePickerDialog.OnTimeChangeListener() {
                    @Override
                    public void onComplete(long minute, long second) {
                        String time = String.format("%02d:%02d", minute, second);
                        mBtnWorkOut.setText(time);
                        mWorkoutTime = (minute * 60 + second) * 1000;
                        reset();
                    }

                    @Override
                    public void onCancel() {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }
                });
                dialog.show((mWorkoutTime / 1000) / 60, (mWorkoutTime / 1000) % 60);
            }
        });

        //Initial RestButton
        mBtnRest = (Button) findViewById(R.id.btnRestTime);
        mBtnRest.setText(timeToString(mRestTime));

        // Set ic_pencil icon to the right of time.
        mBtnRest.setCompoundDrawablesWithIntrinsicBounds(null, null, pencil, null);
        mBtnRest.setCompoundDrawablePadding(TEXT_PADDING);

        mBtnRest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //launch TimerPicker
                TimePickerDialog dialog = new TimePickerDialog(MyActivity.this);
                dialog.setTimeChangeListener(new TimePickerDialog.OnTimeChangeListener() {
                    @Override
                    public void onComplete(long minute, long second) {
                        String time = String.format("%02d:%02d", minute, second);
                        mBtnRest.setText(time);
                        mRestTime = (minute * 60 + second) * 1000;
                        reset();
                    }

                    @Override
                    public void onCancel() {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }
                });
                dialog.show((mRestTime / 1000) / 60, (mRestTime / 1000) % 60);
            }
        });

        //init Spinner
        mtvSeekBar = (TextView) findViewById(R.id.tvSeekBar);
        mSeekBarRounds = (SeekBar) findViewById(R.id.seekBarRounds);
        mSeekBarRounds.setProgress((int) mRounds);
        mtvSeekBar.setText(Integer.toString((int) mRounds + 1));
        mSeekBarRounds.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mStatus == Status.stop)
                    mRounds = i;
                mtvSeekBar.setText(Integer.toString(i + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        }

        );

        //Init StartButton
        mBtnStart = (Button) findViewById(R.id.buttonStart);
        mBtnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mStatus == Status.stop) {
                    //get repeat count
                    mRounds = mSeekBarRounds.getProgress();
                    TimerData timerData = new TimerData(mWorkoutTime, mRestTime, mRounds, null);
                    mPbWorkout.setMax((int) mWorkoutTime);
                    mPbRest.setMax((int) mRestTime);
                    mYogaService.start(timerData);
                } else if (mStatus == Status.pause) {
                    //resume YogaService
                    mYogaService.resume();
                } else if (mStatus == Status.start) {
                    //pause YogaService
                    mYogaService.pause();
                    releaseWakeLock();
                }
            }
        });

        //Init Reset Button
        mBtnReset = (Button) findViewById(R.id.buttonReset);
        mBtnReset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                reset();
                releaseWakeLock();
            }
        });

        //init progressbar
        mPbWorkout = (ProgressBar) findViewById(R.id.pbWorkout);
        mPbRest = (ProgressBar) findViewById(R.id.pbRest);

        //bind YogaService
        bindService(new Intent(MyActivity.this, YogaService.class), mConnection, Context.BIND_AUTO_CREATE);

        //get wakelock
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "YogaTimer");
    }

    private void reset() {
        if (mYogaService != null)
            mYogaService.reset();
        mBtnWorkOut.setText(timeToString(mWorkoutTime));
        mBtnRest.setText(timeToString(mRestTime));
        mBtnStart.setText(R.string.activity_button_start);
        mSeekBarRounds.setProgress((int) mRounds);
        mStatus = Status.stop;
        mSeekBarRounds.setEnabled(true);
        mPbWorkout.setProgress(0);
        mPbRest.setProgress(0);
    }

    private String timeToString(long time) {
        long nMinute = (time / 1000) / 60;
        long nSec = (time / 1000) % 60;
        return String.format("%02d:%02d", nMinute, nSec);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.about_about);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                AboutDialog.show(this);
                break;
            default:
                break;
        }

        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    public void onStop() {
        super.onStop();
        //save preference data
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREFS_WORKOUT, (int) mWorkoutTime);
        editor.putInt(PREFS_REST, (int) mRestTime);
        editor.putInt(PREFS_ROUNDS, (int) mRounds);
        editor.commit();
        Log.d("YogaTimer", "Set workout: " + mWorkoutTime + "rest: " + mRestTime + "rounds: " + mRounds);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseWakeLock();
        reset();
        unbindService(mConnection);
    }

    private void releaseWakeLock() {
        if (mWakeLock.isHeld())
            mWakeLock.release();
    }

    private static enum Status {
        start, pause, stop
    }
}
