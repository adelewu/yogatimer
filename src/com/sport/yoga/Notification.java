package com.sport.yoga;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Hank_Chiu@htc.com
 * Date: 2013/11/2
 */
public class Notification implements TextToSpeech.OnInitListener {
    private Context mContext;
    private ArrayList<Ringtone> mRingtones;
    private Handler mHandler;
    private AlertDialog mDialog;
    private TextToSpeech mTts;
    private MediaPlayer mMediaPlayer = null;
    private Runnable mDialogRunnable = new Runnable() {
        @Override
        public void run() {
            stopAllSounds();
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
    };

    /**
     * Constructor
     *
     * @param context Application context
     */
    public Notification(Context context) {
        mContext = context;
        mRingtones = new ArrayList<Ringtone>();
        mHandler = new Handler();
        mTts = new TextToSpeech(mContext, this);
    }

    public void finish() {
        if (mTts != null) {
            mTts.shutdown();
            mTts = null;
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void notifyUserComplete(long timeout) {
        playAlarmSound();
        showDialog(timeout);
    }

    private Ringtone playSoundInner(int ringtoneType) {
        final Uri ringtoneUri = RingtoneManager.getDefaultUri(ringtoneType);
        final Ringtone ringtone = RingtoneManager.getRingtone(mContext, ringtoneUri);
        if (ringtone == null) {
            Log.e(Constants.TAG, "Cannot get ringtone type: " + ringtoneType);
            return null;
        }

        ringtone.play();
        mRingtones.add(ringtone);
        return ringtone;
    }

    public void playTTS(final String textToSpeak) {
        mTts.speak(textToSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }

    public Ringtone playTickSound(long timeout) {
        final Ringtone ringtone = playSoundInner(RingtoneManager.TYPE_NOTIFICATION);
        if (ringtone != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ringtone.stop();
                }
            }, timeout);
        }
        return ringtone;
    }

    public void playSound(int resourceId) {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
        }

        mMediaPlayer = MediaPlayer.create(mContext, resourceId);
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
            }
        });
    }

    public Ringtone playAlarmSound() {
        return playSoundInner(RingtoneManager.TYPE_ALARM);
    }

    public void stopSound(Ringtone ringtone) {
        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }
    }

    public void stopSound(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
        }
    }

    public void stopAllSounds() {
        for (Ringtone ringtone : mRingtones) {
            stopSound(ringtone);
        }

        stopSound(mMediaPlayer);
    }

    private void showDialog(long timeout) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.notify_title);
        builder.setMessage(R.string.notify_message);
        builder.setNeutralButton(R.string.notify_button_stop, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mHandler.removeCallbacks(mDialogRunnable);
                stopAllSounds();
            }
        });

        mDialog = builder.create();
        mDialog.getWindow().setType((WindowManager.LayoutParams.TYPE_SYSTEM_ALERT));
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        mDialog.show();

        mHandler.postDelayed(mDialogRunnable, timeout);
    }

    private void installTtsData() {
        // Missing data, install it.
        Intent installIntent = new Intent();
        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
        installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(installIntent);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            // Set Language to English by default.
            int result = mTts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e(Constants.TAG, "TextToSpeech is not supported! Result: " + result);
            }
        } else {
            Log.e(Constants.TAG, "TextToSpeech initialization failed!");
        }
    }

}
