package com.sport.yoga;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.app.Notification;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;

/**
 * Created by adele on 2013/12/14.
 */
public class YogaNotifier {
    private static final String TAG = "Yoga";
    private String SEC_STR;
    private static final long MillisecondUnit = 1000;

    private final Context mContext;
    private final NotificationManager mNotifManager;

    public YogaNotifier(Context context) {
        mContext = context;
        mNotifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        SEC_STR = mContext.getString(R.string.notification_second);
    }

    public void cancelAll() {
        mNotifManager.cancelAll();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void updateNotifier(TimerData timerData) {
        if (timerData.status == TimerData.Status.stop) {
            cancelAll();
        }

        final Resources res = mContext.getResources();
        final Notification.Builder builder = new Notification.Builder(mContext);

        final Notification notif;
        builder.setSmallIcon(R.drawable.ic_launcher_alarm);
        builder.setContentTitle(res.getString(R.string.app_name));
        builder.setContentText(getContentText(res, timerData));

        PendingIntent contentIntent = PendingIntent.getActivity(
                mContext,
                0,
                new Intent(mContext, MyActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        notif = builder.build();

        mNotifManager.notify(TAG, 0, notif);
    }

    private CharSequence getContentText(Resources res, TimerData timerData) {
        String remainingText = null;
        if (timerData.status == TimerData.Status.work) {
            remainingText = res.getString(R.string.activity_workout) + "  " +
                    timerData.workoutTime / MillisecondUnit + "  " + SEC_STR;
        } else if (timerData.status == TimerData.Status.rest) {
            remainingText = res.getString(R.string.activity_interval) + "  " +
                    timerData.restTime / MillisecondUnit + "  " + SEC_STR;
        }
        return remainingText;
    }
}
