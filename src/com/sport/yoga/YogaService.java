package com.sport.yoga;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.sport.yoga.Constants.COUNTDOWN_FINIAL;
import static com.sport.yoga.Constants.ALARM_TIMEOUT;

import static com.sport.yoga.TimerData.Status;

public class YogaService extends Service {

    public static enum EventType {
        Start, Tick, Finish, Pause, Complete
    }

    /*
     * Callback list
     */
    private List<TimerListener> mCallback = new ArrayList<TimerListener>();
    /*
     * Time data
     */
    private static TimerData mDefaultTimerData;
    private static TimerData mCurrentTimerData;
    //private static Status mNextStatus;
    private CountdownControl mCountDownTask;
    private Notification mNotifier;
    private YogaNotifier mYogaNotifier;

    /*
     * (non-Javadoc)
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        return new YogaBinder();
    }

    /**
     * Initializes the service when it is first created
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(Constants.TAG, "YogaService onCreate");

        mYogaNotifier = new YogaNotifier(this);
        mYogaNotifier.cancelAll();
        // Sound
        mNotifier = new Notification(this);
    }

    @Override
    public void onDestroy() {
        if (mNotifier != null) {
            mNotifier.finish();
        }
        discardAllTasks();
        mYogaNotifier.cancelAll();
        super.onDestroy();
    }

    public void start(TimerData timerData) {
        Status nextStatus = Status.work;
        mDefaultTimerData = new TimerData(timerData.workoutTime, timerData.restTime, timerData.roundCount, nextStatus);
        mCurrentTimerData = new TimerData(timerData.workoutTime, timerData.restTime, timerData.roundCount, nextStatus);

        // countdown prepare
        prepareStart();
        // Set workout time and start timer immediately
        startNextCountdown(nextStatus);
        // callback
        internalUpdate(EventType.Start, mCurrentTimerData);
    }

    private void prepareStart() {
        /*new CountdownControl(6000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(Constants.TAG, "prepareStart: " + millisUntilFinished);
                if (millisUntilFinished == 5000) mNotifier.playTTS("five");
                else if (millisUntilFinished == 4000) mNotifier.playTTS("four");
                else if (millisUntilFinished == 3000) mNotifier.playTTS("three");
                else if (millisUntilFinished == 2000) mNotifier.playTTS("two");
                else if (millisUntilFinished == 1000) mNotifier.playTTS("one");
            }

            @Override
            public void onFinish() {
                mNotifier.playTTS("ready to start");
            }
        }.start();*/
    }

    public void pause() {
        discardAllTasks();
        // callback
        internalUpdate(EventType.Pause, mCurrentTimerData);
    }

    public void resume() {
        Status status = mCurrentTimerData.status;
        // callback
        internalUpdate(EventType.Start, mCurrentTimerData);
        switch (status) {
            case work:
                mCountDownTask = new Countdown(mCurrentTimerData.workoutTime, status).start();
                break;
            case rest:
                mCountDownTask = new Countdown(mCurrentTimerData.restTime, status).start();
                break;
            case stop:
                break;
        }
    }

    public void reset() {
        mYogaNotifier.cancelAll();
        discardAllTasks();
    }

    private void discardAllTasks() {
        if (mCountDownTask != null) {
            mCountDownTask.cancel();
        } else {
            Log.d(Constants.TAG, "task is null.");
        }
    }

    private void internalUpdate(EventType type, TimerData timerData) {
        mCurrentTimerData = timerData;
        for (TimerListener callback : mCallback) {
            callback.onChange(type, timerData);
        }
    }

    private void startNextCountdown(Status status) {
        switch (status) {
            case work:
                mCurrentTimerData.workoutTime = mDefaultTimerData.workoutTime;
                mCountDownTask = new Countdown(mCurrentTimerData.workoutTime, status).start();
                break;
            case rest:
                mCurrentTimerData.restTime = mDefaultTimerData.restTime;
                mCountDownTask = new Countdown(mCurrentTimerData.restTime, status).start();
                break;
            case stop:
                break;
        }
    }

    private void playAlarm(Status status) {
        if (status == Status.stop) {
            mNotifier.notifyUserComplete(ALARM_TIMEOUT);
            return;
        }
        if (status == Status.rest) {
            mNotifier.playSound(R.raw.rest);
        } else {
            mNotifier.playSound(R.raw.workout);
        }
    }

    private void playNotifySound(long millisUntilFinished) {
        if (millisUntilFinished == 1000 * 3) {
            mNotifier.playSound(R.raw.three);
        } else if (millisUntilFinished == 1000 * 2) {
            mNotifier.playSound(R.raw.two);
        } else if (millisUntilFinished == 1000 * 1) {
            mNotifier.playSound(R.raw.one);
        }
    }

    private TimerData refresh(long millisUntilFinished, Status status, EventType event) {
        TimerData timerData = null;
        Status nextStatus = Status.prepare;

        playNotifySound(millisUntilFinished);

        mCurrentTimerData.status = status;
        if (status == Status.work) {
            mCurrentTimerData.restTime = mDefaultTimerData.restTime;
            mCurrentTimerData.workoutTime = millisUntilFinished;
        } else if (status == Status.rest) {
            mCurrentTimerData.workoutTime = mDefaultTimerData.workoutTime;
            mCurrentTimerData.restTime = millisUntilFinished;
        }

        if (millisUntilFinished == COUNTDOWN_FINIAL) {
            if (mCurrentTimerData.status == Status.work) {
                nextStatus = Status.rest;
                if (mCurrentTimerData.roundCount == 0) {
                    nextStatus = Status.stop;
                }
            }
            // rest : might need to handle status change
            if (status == Status.rest) {
                if (mCurrentTimerData.roundCount > 0) {
                    nextStatus = Status.work;
                    mCurrentTimerData.roundCount = mCurrentTimerData.roundCount - 1;
                } else if (mCurrentTimerData.roundCount == 0) {
                    nextStatus = Status.stop;
                }
            }
            playAlarm(nextStatus);
            startNextCountdown(nextStatus);
        }
        // status bar
        mYogaNotifier.updateNotifier(mCurrentTimerData);
        // callback
        internalUpdate(event, mCurrentTimerData);
        // send complete all tasks event
        if (nextStatus == Status.stop) {
            internalUpdate(EventType.Complete, mDefaultTimerData);
            mYogaNotifier.cancelAll();
        }
        return mCurrentTimerData;
    }

    public class Countdown extends CountdownControl {
        private Status currentStatus;

        Countdown(long millisInFuture, Status status) {
            super(millisInFuture);
            this.currentStatus = status;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            refresh(millisUntilFinished, currentStatus, EventType.Tick);
        }

        @Override
        public void onFinish() {
            refresh(COUNTDOWN_FINIAL, currentStatus, EventType.Finish);
        }

    }

    /*
     * Binder
     */
    public class YogaBinder extends Binder {
        public YogaService getService() {
            return YogaService.this;
        }
    }

    /*
     * Callback handling
     */
    public void addListener(TimerListener listener) {
        if (listener == null) {
            Log.e(Constants.TAG, "Null Listener");
            return;
        }
        mCallback.add(listener);
    }

    public void removeListener(TimerListener listener) {
        if (listener == null) {
            return;
        }

        if (!mCallback.remove(listener)) {
            Log.e(Constants.TAG, "Fail to remove TimerListener");
        }
    }
}
