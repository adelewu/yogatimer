package com.sport.yoga;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created with IntelliJ IDEA.
 * User: Hank_Chiu@htc.com
 * Date: 2013/12/7
 */
public class TimePickerDialog {
    private static final int TIME_MAX_VALUE = 59;
    private static final int TIME_MIN_VALUE = 0;
    private static final long RUNNABLE_WAIT_TIME = 50;
    private static final Object mLock = new Object();
    private Context mContext;
    private Dialog mDialog;
    private Handler mHandler;
    private OnTimeChangeListener mTimeChangeListener;
    private EditText mTextMinute;
    private EditText mTextSecond;
    private int mMinute;
    private int mSecond;
    private boolean mLongPressed;
    private Runnable mIncMinuteRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLongPressed) {
                increaseMinute();
                mHandler.postDelayed(this, RUNNABLE_WAIT_TIME);
            }
        }
    };
    private Runnable mDecMinuteRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLongPressed) {
                decreaseMinute();
                mHandler.postDelayed(this, RUNNABLE_WAIT_TIME);
            }
        }
    };
    private Runnable mIncSecondRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLongPressed) {
                increaseSecond();
                mHandler.postDelayed(this, RUNNABLE_WAIT_TIME);
            }
        }
    };
    private Runnable mDecSecondRunnable = new Runnable() {
        @Override
        public void run() {
            if (mLongPressed) {
                decreaseSecond();
                mHandler.postDelayed(this, RUNNABLE_WAIT_TIME);
            }
        }
    };

    public TimePickerDialog(Context context) {
        mContext = context;
        mHandler = new Handler();
    }

    private boolean isInTimeRange(final int number) {
        return number >= TIME_MIN_VALUE && number <= TIME_MAX_VALUE;
    }

    private int increaseTime(final EditText editText, final int incValue) {
        int value = incValue;
        if (isInTimeRange(value)) {
            ++value;
        }
        if (value > TIME_MAX_VALUE) {
            value = TIME_MIN_VALUE;
        }
        editText.setText(String.valueOf(value));
        return value;
    }

    private int decreaseTime(final EditText editText, final int decValue) {
        int value = decValue;
        if (isInTimeRange(value)) {
            --value;
        }
        if (value < TIME_MIN_VALUE) {
            value = TIME_MAX_VALUE;
        }
        editText.setText(String.valueOf(value));
        return value;
    }

    private void increaseMinute() {
        if (mTextMinute != null) {
            mMinute = increaseTime(mTextMinute, mMinute);
        }
    }

    private void decreaseMinute() {
        if (mTextMinute != null) {
            mMinute = decreaseTime(mTextMinute, mMinute);
        }
    }

    private void increaseSecond() {
        // Do not increase time when it is 59:59.
        if (mMinute == TIME_MAX_VALUE && mSecond == TIME_MAX_VALUE) {
            return;
        }

        if (mTextSecond != null) {
            // Increase minute when it is 59 to 0 second.
            if (mSecond == TIME_MAX_VALUE) {
                increaseMinute();
            }
            mSecond = increaseTime(mTextSecond, mSecond);
        }
    }

    private void decreaseSecond() {
        // Do not decrease time when it is 0:0.
        if (mMinute == TIME_MIN_VALUE && mSecond == TIME_MIN_VALUE) {
            return;
        }

        if (mTextSecond != null) {
            // Decrease minute when it is 0 to 59 second.
            if (mSecond == TIME_MIN_VALUE) {
                decreaseMinute();
            }
            mSecond = decreaseTime(mTextSecond, mSecond);
        }
    }

    private int getTimeValue(final Editable editable) {
        int value = 0;
        try {
            value = Integer.parseInt(editable.toString());
            // Make sure the time on text view will not exceed the range.
            if (value > TIME_MAX_VALUE) {
                editable.replace(0, editable.length(), String.valueOf(TIME_MAX_VALUE), 0, 2);
                value = TIME_MAX_VALUE;
            } else if (value < TIME_MIN_VALUE) {
                editable.replace(0, editable.length(), String.valueOf(TIME_MIN_VALUE), 0, 1);
                value = TIME_MIN_VALUE;
            }
        } catch (NumberFormatException e) {
            Log.v(Constants.TAG, e.getMessage());
        } catch (NullPointerException e) {
            Log.v(Constants.TAG, e.getMessage());
        }
        return value;
    }

    private void initMinutePicker(final Dialog dialog, final long minute) {
        final Button incButton = (Button) dialog.findViewById(R.id.buttonMinuteInc);
        final Button decButton = (Button) dialog.findViewById(R.id.buttonMinuteDec);
        mTextMinute = (EditText) dialog.findViewById(R.id.editTextMinute);

        // Set default minute to text view.
        mMinute = (int) minute;
        mTextMinute.setText(String.valueOf(mMinute));

        mTextMinute.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mMinute = getTimeValue(editable);
            }

        });

        incButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                increaseMinute();
            }

        });

        incButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                synchronized (mLock) {
                    mLongPressed = true;
                    mHandler.post(mIncMinuteRunnable);
                }
                return true;
            }

        });

        incButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL ||
                        motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    synchronized (mLock) {
                        mLongPressed = false;
                        mHandler.removeCallbacks(mIncMinuteRunnable);
                    }
                }
                return false;
            }
        });

        decButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                decreaseMinute();
            }
        });

        decButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                synchronized (mLock) {
                    mLongPressed = true;
                    mHandler.post(mDecMinuteRunnable);
                }
                return true;
            }

        });

        decButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL ||
                        motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    synchronized (mLock) {
                        mLongPressed = false;
                        mHandler.removeCallbacks(mDecMinuteRunnable);
                    }
                }
                return false;
            }
        });
    }

    private void initSecondPicker(final Dialog dialog, final long second) {
        final Button incButton = (Button) dialog.findViewById(R.id.buttonSecondInc);
        final Button decButton = (Button) dialog.findViewById(R.id.buttonSecondDec);
        mTextSecond = (EditText) dialog.findViewById(R.id.editTextSecond);

        // Set default second to text view.
        mSecond = (int) second;
        mTextSecond.setText(String.valueOf(mSecond));

        mTextSecond.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mSecond = getTimeValue(editable);
            }

        });

        incButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                increaseSecond();
            }

        });

        incButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                synchronized (mLock) {
                    mLongPressed = true;
                    mHandler.post(mIncSecondRunnable);
                }
                return true;
            }

        });

        incButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL ||
                        motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    synchronized (mLock) {
                        mLongPressed = false;
                        mHandler.removeCallbacks(mIncSecondRunnable);
                    }
                }
                return false;
            }
        });

        decButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                decreaseSecond();
            }

        });

        decButton.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                synchronized (mLock) {
                    mLongPressed = true;
                    mHandler.post(mDecSecondRunnable);
                }
                return true;
            }

        });

        decButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL ||
                        motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    synchronized (mLock) {
                        mLongPressed = false;
                        mHandler.removeCallbacks(mDecSecondRunnable);
                    }
                }
                return false;
            }
        });
    }

    public void show() {
        show(0, 0);
    }

    public void show(long minute, long second) {
        if (mDialog == null) {
            mDialog = new Dialog(mContext);
        }

        mDialog.setTitle(R.string.dialog_title);
        mDialog.setContentView(R.layout.numberpicker_dialog);

        final Button buttonCancel = (Button) mDialog.findViewById(R.id.buttonCancel);
        final Button buttonSet = (Button) mDialog.findViewById(R.id.buttonSet);

        initMinutePicker(mDialog, minute);
        initSecondPicker(mDialog, second);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimeChangeListener.onCancel();
                mDialog.dismiss();
            }
        });

        buttonSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimeChangeListener.onComplete(mMinute, mSecond);
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void setTimeChangeListener(final OnTimeChangeListener listener) {
        mTimeChangeListener = listener;
    }

    public interface OnTimeChangeListener {
        public void onComplete(long minute, long second);

        public void onCancel();
    }

}
