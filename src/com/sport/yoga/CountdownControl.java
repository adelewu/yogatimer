package com.sport.yoga;

import android.os.Handler;
import android.os.Message;

/**
 * Created by adele on 2013/12/2.
 */
public abstract class CountdownControl {

    public final long CountdownInterval = 1000;
    /**
     * Millis since epoch when alarm should stop.
     */
    private final long mMillisInFuture;

    private long mStopLeftTime;

    /**
     * @param millisInFuture The number of millis in the future from the call
     *                       to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                       is called.
     */
    public CountdownControl(long millisInFuture) {
        mMillisInFuture = millisInFuture;
    }

    /**
     * Cancel the countdown.
     */
    public final void cancel() {
        mHandler.removeMessages(MSG);
    }

    /**
     * Start the countdown.
     */
    public synchronized final CountdownControl start() {
        if (mMillisInFuture <= 0) {
            onFinish();
            return this;
        }
        mStopLeftTime = mMillisInFuture;
        // To avoid sending onTick immediately
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG), CountdownInterval);
        //mHandler.sendMessage(mHandler.obtainMessage(MSG));
        return this;
    }


    /**
     * Callback fired on regular interval.
     *
     * @param millisUntilFinished The amount of time until finished.
     */
    public abstract void onTick(long millisUntilFinished);

    /**
     * Callback fired when the time is up.
     */
    public abstract void onFinish();


    private static final int MSG = 1;


    // handles counting down
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            synchronized (CountdownControl.this) {
                final long millisLeft = mStopLeftTime - 1000;

                if (millisLeft <= 0) {
                    onFinish();
                } else if (millisLeft < CountdownInterval) {
                    // no tick, just delay until done
                    sendMessageDelayed(obtainMessage(MSG), CountdownInterval);
                } else {
                    onTick(millisLeft);
                    sendMessageDelayed(obtainMessage(MSG), CountdownInterval);
                }
                mStopLeftTime = millisLeft;
            }
        }
    };
}
