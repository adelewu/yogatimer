package com.sport.yoga;

public class TimerData {
    public static enum Status {
        prepare, work, rest, stop
    }

    public long workoutTime;
    public long restTime;
    public long roundCount;
    public Status status;

    public TimerData(long workoutTime, long restTime, long roundCount, Status status) {
        this.workoutTime = workoutTime;
        this.restTime = restTime;
        this.roundCount = roundCount;
        this.status = status;
    }
}
