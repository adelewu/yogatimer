package com.sport.yoga;

public class Constants {

    public static final String TAG = "RepeatAlarmDebug";
    public static final long COUNTDOWN_INTERVAL = 1000;
    public static final long COUNTDOWN_FINIAL = 0;

    public static final long TICK_TIMEOUT = 2000;
    public static final long ALARM_TIMEOUT = 10000;
    public static final int CLOSE_APP_TIMEOUT = 4000;
}
