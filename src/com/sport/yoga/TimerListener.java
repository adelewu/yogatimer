package com.sport.yoga;

public interface TimerListener {

    public void onChange(YogaService.EventType type, TimerData timerData);

}
